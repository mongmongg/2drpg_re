﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ConvertData
{
    #region singleton
    private static ConvertData _instance;
    public static ConvertData Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new ConvertData();
            }
            return _instance;
        }

    }
    #endregion

    [Header("ITEM")]
    TextAsset itemData_JSON;
    public Dictionary<int, RawDataItem> dict_ItemDataJSON;

    // [Header("CRAFT")]
    // TextAsset craftData_JSON;
    // public Dictionary<int, RawDataCraft> dict_CraftDataJSON;

    [Header("DIALOGUE")]
    TextAsset dialogueData_JSON;
    public Dictionary<int, RawDataDialogue> dict_DialogueDataJSON;

    [Header("QUEST")]
    TextAsset questData_JSON;
    public Dictionary<int, RawDataQuest> dict_QuestDataJSON;

    public ConvertData()
    {
        UseJSONDialogueData();
        UseJsonItemData();
        UseJSONQuestData();
        // UseJSONCraftData();

        // 
    }
    public void UseJsonItemData()
    {
        itemData_JSON = Resources.Load<TextAsset>("Data/JSONItemData");
        dict_ItemDataJSON = JsonUtility.FromJson<RawDataItemList>(itemData_JSON.text)
            ._rawdataItemList.ToDictionary(item_data_JSON => item_data_JSON.IDItem);
    }
    // public void UseJSONCraftData()
    // {
    //     craftData_JSON = Resources.Load<TextAsset>("Data/JSONCraftData");
    //     dict_CraftDataJSON = JsonUtility.FromJson<RawDataCraftList>(craftData_JSON.text)
    //         ._rawDataCraftList.ToDictionary(craftData_JSON => craftData_JSON.CraftID);
    // }
    public void UseJSONDialogueData()
    {
        dialogueData_JSON = Resources.Load<TextAsset>("Data/JSONDialogueData");
        dict_DialogueDataJSON = JsonUtility.FromJson<RawdataDialogueList>(dialogueData_JSON.text)
            ._rawdataDialogue.ToDictionary(dialogueData_JSON => dialogueData_JSON.IDnpc);
    }
    public void UseJSONQuestData()
    {
        questData_JSON = Resources.Load<TextAsset>("Data/JSONQuestData");
        dict_QuestDataJSON = JsonUtility.FromJson<RawDataQuestList>(questData_JSON.text)
        ._rawdataQuest.ToDictionary(questData_JSON => questData_JSON.IDquest);
    }

}
