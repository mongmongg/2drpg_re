﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetAITarget : MonoBehaviour
{
    Pathfinding.AIDestinationSetter aIDestination;
    // Start is called before the first frame update
    void Start()
    {
        aIDestination = GetComponent<Pathfinding.AIDestinationSetter>();
        aIDestination.target = PlayerController.Instance.player.transform;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
