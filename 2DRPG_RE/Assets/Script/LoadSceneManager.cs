﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneManager : SingletonBehaviour<LoadSceneManager>
{
    public List<PlayerSpawnPosition> list_playerPt;
    // Start is called before the first frame update
    void Start()
    {
        LoadPlayerPosition();
    }
    // Update is called once per frame
    public void LoadPlayerPosition()
    {
        list_playerPt = new List<PlayerSpawnPosition>();
        list_playerPt = FindObjectsOfType<PlayerSpawnPosition>().ToList();
    }
    public void LoadToScene(string m_sceneName)
    {
        Debug.Log("LoadToScene");
        PlayerPrefs.SetString("SceneNumber", SceneManager.GetActiveScene().name);
        SceneManager.LoadScene(m_sceneName);
    }
    public void SpawnPlayerToPosition(string _name)
    {
        if (SceneManager.GetActiveScene().buildIndex != 0)
        {
            List<PlayerSpawnPosition> newList = new List<PlayerSpawnPosition>();
            foreach (PlayerSpawnPosition a in list_playerPt)
            {
                newList.Add(a);
            }
            PlayerSpawnPosition playerSpawn = newList.Find((x) => x.position_Name == _name);
            PlayerSpawnPosition defaultPos = newList.Find((x) => x.position_default == true);
            if (playerSpawn != null)
            {
                PlayerController.Instance.player.transform.localPosition = playerSpawn.transform.position;
            }
            else
            {
                PlayerController.Instance.player.transform.localPosition = defaultPos.transform.position;
            }

        }
    }
}
