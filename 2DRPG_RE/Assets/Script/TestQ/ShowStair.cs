﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowStair : MonoBehaviour
{
    public GameObject hiddenStair;
    private void Start()
    {
        if (PlayerPrefs.GetInt("Victoria") == 0)
        {
            hiddenStair.SetActive(false);
        }
    }
    void Update()
    {
        if (PlayerPrefs.GetInt("Victoria") == 1)
        {
            hiddenStair.SetActive(true);
        }
    }
}
