﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DialogueTextBox : MonoBehaviour
{
    public TextMeshProUGUI textMeshProUGUI;
    public Image playerImage;
    public Image npcImage;
    //RawDataDialogue rawDataDialogue;
    void Awake()
    {
        textMeshProUGUI = GetComponentInChildren<TextMeshProUGUI>();
        //playerImage = GetComponent<Image>();
        //npcImage = GetComponent<Image>();
    }
    public void SetDialogue(RawDataDialogue m_dialogue)
    {
        textMeshProUGUI.text = m_dialogue.Dialogue;
        if (m_dialogue.CharacterName == "Alicia")
        {
            playerImage.sprite = Resources.Load<Sprite>("Dialogue/NpcUI Icon/" + m_dialogue.CharacterName);
            playerImage.gameObject.SetActive(true);
            npcImage.gameObject.SetActive(false);
        }
        else
        {
            playerImage.gameObject.SetActive(false);
            npcImage.sprite = Resources.Load<Sprite>("Dialogue/NpcUI Icon/" + m_dialogue.CharacterName);
            npcImage.gameObject.SetActive(true);
        }

    }

}
