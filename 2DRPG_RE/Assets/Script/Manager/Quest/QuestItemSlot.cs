﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class QuestItemSlot : MonoBehaviour
{
    public TextMeshProUGUI itemName;
    public TextMeshProUGUI amountItem;
    int inventoryItemAmt;
    int questAmout;
    public bool isComplete;
    // Start is called before the first frame update
    public void Initialization(string _name, int _amount)
    {
        itemName.text = _name;
        //amountItem.text = _amount.ToString();
        questAmout = _amount;
        UpdateItemSlot();
    }

    // Update is called once per frame
    public void UpdateItemSlot()
    {
        Inventory inventory = GameController.Instance.inventory;
        foreach (InventorySlot item in inventory.dict_inventorySlot.Values)
        {
            Debug.Log(item.itemInfo.itemname);
            Debug.Log("itemName: " + itemName.text);
            if (item.itemInfo.itemname == itemName.text)
            {
                inventoryItemAmt = item.amount;
            }
        }
        amountItem.text = inventoryItemAmt.ToString() + " / " + questAmout.ToString();
        CheckQuestIsComplete();
    }
    void CheckQuestIsComplete()
    {
        if (inventoryItemAmt >= questAmout)
        {
            isComplete = true;
        }
        else { isComplete = false; }
    }

}
