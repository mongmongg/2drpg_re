﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class QuestDisplay : MonoBehaviour
{
    public GameObject questDisplay;
    public TextMeshProUGUI titleQuest;
    public TextMeshProUGUI detailQuest;
    public Transform questItemslot_position;
    private void OnEnable()
    {
        if (SceneBookStatus.Instance.statusInfo_UI != null)
        {
            CheckQuest();
        }
    }

    private void CheckQuest()
    {
        if (GameController.Instance.questManager.rawDataQuest.Count == 0)
        {
            if (questDisplay != null)
            {
                questDisplay.SetActive(false);
            }
        }
        else
        {
            if (questDisplay != null)
            {
                questDisplay.SetActive(true);
            }
        }

    }
}
