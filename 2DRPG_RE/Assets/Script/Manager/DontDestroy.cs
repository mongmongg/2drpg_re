﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroy : MonoBehaviour
{
    public static List<string> singletons = new List<string>();

    void Awake()
    {
        string gameObjectIdentifier = gameObject.name;
        if (!singletons.Contains(gameObjectIdentifier))
        {
            singletons.Add(gameObjectIdentifier);
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            DestroyImmediate(gameObject);
        }

    }
}
