﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DialogueEffect : MonoBehaviour
{
    Button m_btn;
    public void CheckEffect(RawDataDialogue rawDataDialogue)
    {
        if (rawDataDialogue.Effect == "LoadScene")
        {
            m_btn = UIGamePlay.Instance.interactive_ControlPanel.GetComponentInChildren<Button>();
            m_btn.GetComponent<Button>().enabled = false;
            StartCoroutine(WaitToLoadScene(rawDataDialogue));
        }
        if (rawDataDialogue.Effect == "Fade")
        {
            StartCoroutine(WaitToLoadMap());
        }
    }
    IEnumerator WaitToLoadScene(RawDataDialogue rawDataDialogue)
    {
        yield return new WaitForSeconds(1f);
        UIGamePlay.Instance.fadeImage.FadeOut();
        GameController.Instance.dialogueManager.EndDialogue();
        yield return new WaitForSeconds(0.5f);
        PlayerPrefs.SetString("SceneNumber", SceneManager.GetActiveScene().name);
        SceneManager.LoadScene(rawDataDialogue.LoadToScene);
        m_btn.GetComponent<Button>().enabled = true;
    }
    IEnumerator WaitToLoadMap()
    {
        CutSceneMap3 cutScene = FindObjectOfType<CutSceneMap3>();
        yield return new WaitForSeconds(1f);
        UIGamePlay.Instance.fadeImage.FadeOut();
        yield return new WaitForSeconds(0.5f);
        cutScene.DisplayNextMap();
        UIGamePlay.Instance.fadeImage.FadeIn();


    }

}
