﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[AddComponentMenu("2DRPG/Manager/SaveManager")]
public class GameSaveManager : MonoBehaviour
{
    public SaveData saveData;

    public void SaveData()
    {
        SaveInventory();
        SavePlayerHealth();
        SaveCurrentDialogue();
        SaveCurrentQuest();
        SaveSceneAndPosition();
        GameSave.Instance.SaveDataManager(saveData);
    }
    public void LoadData()
    {
        GameSave.Instance.LoadDataManager();
        saveData = GameSave.Instance.saveData;
        LoadInventory(saveData);
        LoadPlayerHealth(saveData);
        LoadCurrentDialogue(saveData);
        LoadCurrentQuest(saveData);
        LoadSceneAndPosition();
        saveData = GameSave.Instance.saveData;
    }
    // Start is called before the first frame update
    public void SaveInventory()
    {
        saveData.itemDataName = new List<string>();
        saveData.itemDataQuantity = new List<int>();
        foreach (InventorySlot slot in GameController.Instance.inventory.dict_inventorySlot.Values)
        {
            saveData.itemDataName.Add(slot.itemInfo.itemname);
            saveData.itemDataQuantity.Add(slot.amount);
        }
    }
    public void LoadInventory(SaveData saveData)
    {
        GameController.Instance.inventoryManager.ClearInventoryManager();
        for (int i = 0; i < saveData.itemDataName.Count; i++)
        {
            ItemInfo item = Resources.Load<ItemInfo>("Item/" + saveData.itemDataName[i]);
            GameController.Instance.inventory.Add(item, saveData.itemDataQuantity[i]);
        }
    }
    public void SavePlayerHealth()
    {
        saveData.p_HP = PlayerController.Instance.player.GetComponent<Health>().CurrentHealth;
    }
    public void LoadPlayerHealth(SaveData saveData)
    {
        PlayerController.Instance.player.GetComponent<Health>().SetHealth(saveData.p_HP);
    }
    public void SaveCurrentDialogue()
    {
        saveData.currentDialogue = GameController.Instance.dialogueManager.currentNoDialogue;
        Debug.Log(saveData.currentDialogue);
    }
    public void LoadCurrentDialogue(SaveData saveData)
    {
        GameController.Instance.dialogueManager.currentNoDialogue = saveData.currentDialogue;
    }
    public void SaveCurrentQuest()
    {
        saveData.currentQuest = GameController.Instance.questManager.currentQuest;
    }
    public void LoadCurrentQuest(SaveData saveData)
    {
        GameController.Instance.questManager.FindQuestInDATA(saveData.currentQuest);
        GameController.Instance.questManager.AddQuest();
    }
    public void SaveSceneAndPosition()
    {
        saveData.sceneIndex = SceneManager.GetActiveScene().buildIndex;
        //saveData.playerPositionX = PlayerController.Instance.player.transform.position.x;
        //saveData.playerPositionY = PlayerController.Instance.player.transform.position.y;
    }
    public void LoadSceneAndPosition()
    {
        SceneManager.LoadScene(saveData.sceneIndex);        
        //PlayerController.Instance.SetPositionOnLoadData(saveData.playerPositionX, saveData.playerPositionY);
    }

}
