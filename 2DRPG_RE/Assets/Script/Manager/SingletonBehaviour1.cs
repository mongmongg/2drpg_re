﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SingletonBehaviour1<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T instance;
    public static T Instance
    {
        get
        {
            if (instance == null)
            {
                instance = (T)FindObjectOfType(typeof(T));
                if (instance == null)
                {
                    Debug.LogError("An instance of " + typeof(T) + "is needed in the scene, but there is none");
                }
            }
            return instance;
        }
    }
}

