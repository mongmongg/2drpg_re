﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SingletonBehaviour<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T instance;
    private static object _lock = new object();
    public static T Instance
    {
        get
        {
            // if (applicationIsQuitting)
            // {
            //     Debug.LogWarning("[MonoSingleton] Instance '" + typeof(T) +
            //     "' already destroyed on application quit." +
            //     " Won't create again - returning null.");
            //     return null;
            // }
            if (instance == null)
            {
                instance = (T)FindObjectOfType(typeof(T));
                if (FindObjectsOfType(typeof(T)).Length > 1)
                {
                    Debug.LogError("More Than 1 Singleton" + (typeof(T).ToString()));
                    return instance;
                }

                if (instance == null)
                {
                    GameObject singleton = new GameObject();
                    instance = singleton.AddComponent<T>();
                    singleton.name = "(Singleton) " + typeof(T).ToString();
                    DontDestroyOnLoad(singleton);
                }
                else
                {
                    //singletons.Add(typeof(T).ToString());                    
                    Debug.Log("[MonoSingleton] Using instance already created: " +
                        instance.gameObject.name);
                    //Test(instance.gameObject);
                }
                // if (instance == null)
                // {
                //     Debug.LogError("An instance of " + typeof(T) + "is needed in the scene, but there is none");
                // }
            }
            return instance;
        }
    }
    private static bool applicationIsQuitting = false;
}
