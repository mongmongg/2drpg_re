﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
//using Newtonsoft.Json;

public class GameSave
{
    #region singleton
    private static GameSave _getValue;
    public static GameSave Instance
    {
        get
        {
            if (_getValue == null)
            {
                _getValue = new GameSave();
            }
            return _getValue;
        }
    }
    #endregion
    public SaveData saveData;
    public void CreateSaveGameobject()
    {
        Debug.Log("----------New Save----------");
        SaveData save = new SaveData();
        save.itemDataName = new List<string>();
        save.itemDataQuantity = new List<int>();       
        SaveDataManager(save);
    }
    public void SaveDataManager(SaveData m_save)
    {
        saveData = m_save;
        string jsonFile = JsonConvert.SerializeObject(m_save);
        Debug.Log("---------SAVE----------" + jsonFile);
        PlayerPrefs.SetString("savefile", jsonFile);
    }
    public void LoadDataManager()
    {
        string datasave = PlayerPrefs.GetString("savefile");
        if (datasave == "")
        {
            Debug.LogWarning("datasaving = null >> Not Found");
            //createSaveGamobject();
        }
        else
        {
            saveData = JsonConvert.DeserializeObject<SaveData>(PlayerPrefs.GetString("savefile"));
            Debug.LogWarning("Found: " + datasave);
        }
    }
}
