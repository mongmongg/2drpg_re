﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("2DRPG/Character/Movement")]
public class Movement : Ability
{
    public float MovementSpeed { get; set; }
    [Header("Speed")]
    public float WalkSpeed = 5f;
    Vector3 raycast_Direction;
    // CharacterDash characterDash;
    int layerMask1 = 8;
    int layerMask2 = 9;
    int layerMask3 = 10;
    int layerMask;
    int layerMask_exceptPlayer;
    protected override void Initialization()
    {
        base.Initialization();
        MovementSpeed = WalkSpeed;
        //characterDash = GetComponent<CharacterDash>();
        layerMask = ~((1 << layerMask1) | (1 << layerMask2) | (1 << layerMask3));
        layerMask_exceptPlayer = ~((1 << layerMask1));
    }

    public override void ProcessAbility()
    {
        HandleHorizontalMovement();
        DashDistance();
    }
    public override void EarlyProcessAbility()
    {
        if (CanMove())
        {
            rb.MovePosition(rb.position + m_movement * MovementSpeed * Time.deltaTime);
        }
    }
    protected virtual void HandleHorizontalMovement()
    {
        m_movement.x = Input.GetAxisRaw("Horizontal");
        m_movement.y = Input.GetAxisRaw("Vertical");
        if (m_movement.sqrMagnitude != 0)
        {
            animator.SetFloat("Horizontal", m_movement.x);
            animator.SetFloat("Vertical", m_movement.y);
        }
        animator.SetFloat("Speed", m_movement.sqrMagnitude);
    }
    public void DashDistance()
    {
        raycast_Direction = new Vector2(m_movement.x, m_movement.y);
        RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, raycast_Direction, characterDash.dashDistance, layerMask);
        if (hitInfo.collider != null)
        {
            // Debug.Log("distance" + hitInfo.collider.name + hitInfo.distance);
            if (hitInfo.distance < 1)
            {
                characterDash.SetDistance(0);
            }
            else { characterDash.SetDistance(hitInfo.distance - 0.5f); }
        }
        else
        {
            Debug.DrawLine(transform.position, transform.position + raycast_Direction * characterDash.dashDistance, Color.green);
            characterDash.SetDistance(characterDash.dashDistance);
        }
    }
    bool CanMove()
    {
        if (shield.useSkill)
        {
            return false;
        }
        if (PlayerController.Instance.player.GetComponent<Health>().RespawnAtInitialLocation)
        {
            return false;
        }
        if (PlayerController.Instance.player.GetComponent<Health>().CurrentHealth <= 0) { return false; }
        else { return true; }
    }
}



