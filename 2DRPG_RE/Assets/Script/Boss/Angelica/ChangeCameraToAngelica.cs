﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCameraToAngelica : MonoBehaviour
{
    Collider2D angelicaCollide;
    Angelica angelica;
    public bool disableGameobj;
    public bool changeBackToPlayer;
    private void OnTriggerEnter2D(Collider2D other)
    {
        angelicaCollide = other;
        ChangeCam();
    }
    void ChangeCam()
    {
        if (CheckIfAngelica())
        {
            if (changeBackToPlayer)
            {
                GameController.Instance.cameraMovement.angelicaBoss = false;            
            }
            else
            {
                GameController.Instance.cameraMovement.angelicaBoss = true;
                if (disableGameobj)
                {
                    this.transform.parent.gameObject.SetActive(false);
                    Angelica.Instance.GetComponent<AngelicaSkill>().enabled = true;
                    Angelica.Instance.wall.SetActive(true);
                }
            }
        }
    }
    bool CheckIfAngelica()
    {
        angelica = angelicaCollide.GetComponent<Angelica>();
        if (angelica != null) { return true; }
        return false;
    }
}
