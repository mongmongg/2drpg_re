﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AngelicaSkill : MonoBehaviour
{
    public enum SkillPattern { RandomPoint, Line }
    public SkillPattern skillPattern;
    [Header("Skill 1")]
    public GameObject bullet;
    public Transform spawnBullet;
    public GameObject spawnPointRand;
    public int indexLenge = 4;
    public static List<AngelicaBulletSpawnPoint> currentRandNumber;
    public static List<AngelicaBulletSpawnPoint> spawnPointsSkill_rand;

    [Header("Skill 2")]
    public GameObject spawnPointLine;
    public List<AngelicaBulletSpawnPoint> spawnPointsSkill_line;
    float randNumber;

    Color redColor;
    Color whiteColor;
    float targetAlpha = 70;

    void OnEnable()
    {
        StartCoroutine(RandomSkill(0f));
    }
    private void OnDisable()
    {

    }
    void Awake()
    {
        spawnPointsSkill_rand = new List<AngelicaBulletSpawnPoint>();
        spawnPointsSkill_rand = spawnPointRand.GetComponentsInChildren<AngelicaBulletSpawnPoint>().ToList();
        spawnPointsSkill_line = new List<AngelicaBulletSpawnPoint>();
        spawnPointsSkill_line = spawnPointLine.GetComponentsInChildren<AngelicaBulletSpawnPoint>().ToList();
        currentRandNumber = new List<AngelicaBulletSpawnPoint>();

        foreach (AngelicaBulletSpawnPoint a in spawnPointsSkill_rand)
        {
            a.gameObject.SetActive(false);
        }
        foreach (AngelicaBulletSpawnPoint a in spawnPointsSkill_line)
        {
            a.gameObject.SetActive(false);
        }

    }
    void UseSkill()
    {
        if (skillPattern == SkillPattern.RandomPoint)
        {
            spawnPointRand.SetActive(true);
            spawnPointLine.SetActive(false);
            FireRandomPoint();
        }
        else if (skillPattern == SkillPattern.Line)
        {
            spawnPointRand.SetActive(false);
            spawnPointLine.SetActive(true);
            FireLine();
        }
    }
    void FireRandomPoint()
    {
        StartCoroutine(RandomSkill(5f));
        foreach (AngelicaBulletSpawnPoint a in spawnPointsSkill_rand)
        {
            a.gameObject.SetActive(false);
        }

        for (int i = 0; i < indexLenge; i++)
        {
            generateRandomNumber();
            currentRandNumber[i].gameObject.SetActive(true);
            SpriteRenderer sprite = currentRandNumber[i].gameObject.GetComponent<SpriteRenderer>();
            StartCoroutine(AlertBeforeAttack(sprite, currentRandNumber[i].transform));
        }
        currentRandNumber.Clear();
    }
    void FireLine()
    {
        StartCoroutine(RandomSkill(5f));
        foreach (AngelicaBulletSpawnPoint a in spawnPointsSkill_line)
        {
            a.gameObject.SetActive(false);
        }
        AngelicaBulletSpawnPoint rand_spawnPoint = spawnPointsSkill_line[Random.Range(0, spawnPointsSkill_line.Count)];
        rand_spawnPoint.gameObject.SetActive(true);
        SpriteRenderer sprite = rand_spawnPoint.gameObject.GetComponent<SpriteRenderer>();
        StartCoroutine(AlertBeforeAttack(sprite, rand_spawnPoint.R_spawnbullet));
    }
    IEnumerator AlertBeforeAttack(SpriteRenderer spriteRenderer, Transform point)
    {
        spriteRenderer.enabled = true;
        spriteRenderer.color = new Color(255, 0, 0, 0.5f);
        Debug.Log("Red");
        yield return new WaitForSeconds(0.5f);
        spriteRenderer.color = new Color(255, 255, 255, 0.5f);
        yield return new WaitForSeconds(0.5f);
        spriteRenderer.color = new Color(255, 0, 0, 0.5f);
        yield return new WaitForSeconds(0.5f);
        spriteRenderer.color = new Color(255, 255, 255, 0.5f);
        yield return new WaitForSeconds(0.5f);
        spriteRenderer.enabled = false;
        yield return new WaitForSeconds(0.2f);
        SpawnBullet(point);
    }
    void SpawnBullet(Transform point)
    {
        if (skillPattern == SkillPattern.RandomPoint)
        {
            GameObject a = Instantiate(bullet, spawnBullet, false);
            LeanTween.move(a, point, 0.3f);
            a.GetComponent<DamageItem>().enabled = false;
            Collider2D collider = point.GetComponent<Collider2D>();
            a.GetComponent<AngelicaBullet>().m_coll = collider;
        }
        else if (skillPattern == SkillPattern.Line)
        {
            GameObject m_bullet = Instantiate(bullet, point, false);
            m_bullet.GetComponent<Bullet>().enabled = true;
            m_bullet.GetComponent<DamageItem>().destroyObject = false;
            m_bullet.GetComponent<Bullet>().SetMoveDirection(Vector2.right);
        }
    }
    IEnumerator RandomSkill(float m_time)
    {
        yield return new WaitForSeconds(m_time);
        randNumber = Random.Range(0f, 2f);
        Debug.Log(randNumber);
        if (0f < randNumber && randNumber < 1.0f)
        {
            skillPattern = SkillPattern.RandomPoint;
            UseSkill();
        }
        else if (1.0f <= randNumber && randNumber < 2.0f)
        {
            skillPattern = SkillPattern.Line;
            UseSkill();
        }
    }
    public static AngelicaBulletSpawnPoint generateRandomNumber()
    {
        AngelicaBulletSpawnPoint result = spawnPointsSkill_rand[Random.Range(0, spawnPointsSkill_rand.Count)];
        foreach (AngelicaBulletSpawnPoint a in currentRandNumber)
        {
            if (a == result)
            {
                return generateRandomNumber();
            }
        }
        currentRandNumber.Add(result);
        return result;
    }
}
