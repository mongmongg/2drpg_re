﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AngelicaBullet : MonoBehaviour
{
    public Collider2D _collider;
    public Collider2D m_coll;
    AngelicaBulletSpawnPoint angelicaBulletSpawnPoint;
    AngelicaSkill angelicaSkill;
    private void OnTriggerEnter2D(Collider2D other)
    {
        _collider = other;
        EnableDamage();
    }
    void EnableDamage()
    {
        if (CheckIfInPosition())
        {
            Debug.Log("Open");
            this.GetComponent<DamageItem>().enabled = true;
            if (angelicaSkill.skillPattern == AngelicaSkill.SkillPattern.RandomPoint)
            {
                StartCoroutine(DestroyBullet());
            }
        }
    }
    public bool CheckIfInPosition()
    {
        angelicaBulletSpawnPoint = _collider.GetComponent<AngelicaBulletSpawnPoint>();
        angelicaSkill = _collider.GetComponentInParent<AngelicaSkill>();
        if (angelicaBulletSpawnPoint == null) { return false; }
        if (_collider == m_coll) { return true; }
        return false;
    }
    IEnumerator DestroyBullet()
    {
        Debug.Log("Destroy");
        yield return new WaitForSeconds(.3f);
        if (this.gameObject != null)
        {
            Destroy(this.gameObject);
        }
    }
}
