﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VictoriaSkillPattern : MonoBehaviour
{
    public enum SkillPattern { ShootToPlayer, Circle, Shield }
    public SkillPattern skillPattern;
    protected SkillPattern currentSkill;
    BulletFireType bulletFireType;
    [SerializeField]
    private int bulletAmout = 8;
    [SerializeField]
    private float startAngle = 0f, endAngle = 360f;
    private Vector2 bulletMoveDirection;
    public GameObject player;
    public GameObject victoria_shield;
    // Start is called before the first frame update
    void Start()
    {
        bulletFireType = GetComponent<BulletFireType>();
        player = PlayerController.Instance.player;
        victoria_shield = GetComponentInChildren<DamageItem>().gameObject;
        victoria_shield.SetActive(false);
        UseSkill();
    }
    void Update()
    {
        if (ChangeSkill())
        {
            UseSkill();
        }
    }
    bool ChangeSkill()
    {
        if (skillPattern != currentSkill)
        {
            return true;
        }
        else return false;
    }
    void UseSkill()
    {
        currentSkill = skillPattern;
        if (skillPattern == SkillPattern.Circle)
        {
            CancelInvoke();            
            InvokeRepeating("FireCircleType", 0f, 2f);
        }
        else if (skillPattern == SkillPattern.ShootToPlayer)
        {
            CancelInvoke();
            InvokeRepeating("FireToPlayerPosition", 0, 2f);            
        }
        else
        {
            CancelInvoke();
            victoria_shield.SetActive(true);
            InvokeRepeating("Shield", 0, 5f);     
        }
    }
    void FireCircleType()
    {
        bulletFireType.FireCircleType(startAngle, endAngle, bulletAmout);
        victoria_shield.SetActive(false);
    }
    void FireToPlayerPosition()
    {
        bulletFireType.FireToPlayerPosition(player,0);
        victoria_shield.SetActive(false);
    }
    void Shield()
    {
        Vector3 dashPosition = player.transform.position;        
        LeanTween.move(this.gameObject, dashPosition, 0.3f);
    }
}
