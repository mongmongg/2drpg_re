﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrystalInMap : MonoBehaviour
{
    public Victoria victoria;
    Collider2D red_collide;
    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.GetInt("Victoria") == 1)
        {
            this.gameObject.SetActive(false);
            GetComponent<CrystalInMap>().enabled = false;
        }

    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        red_collide = other;
        DestroyCrystal();
    }
    void DestroyCrystal()
    {
        if (CheckIfRedBall())
        {
            victoria = Victoria.Instance;
            victoria.currentHP = victoria.currentHP - 2;
            Debug.Log(victoria.currentHP);
            this.gameObject.SetActive(false);
        }
    }
    bool CheckIfRedBall()
    {
        Bullet bullet = red_collide.GetComponent<Bullet>();
        if (bullet == null) { return false; }
        if (bullet.bulletType == Bullet.BulletType.red) { return true; }
        return false;
        //if(bullet.)
    }
}
