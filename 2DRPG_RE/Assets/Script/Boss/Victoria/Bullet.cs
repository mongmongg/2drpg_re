﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public enum BulletType { red, purple, none }
    private Vector2 moveDirection;
    private float moveSpeed;
    [Header("Victoria")]
    public bool isVicBull;
    public float vic_moveSpeed = 5f;
    public BulletType bulletType;
    [Header("Angelica")]
    public bool isAngelBull;
    public float angel_moveSpeed = 7f;
    [Header("Bow")]
    public bool isBow;
    public float bow_moveSpeed = 5f;


    private void OnEnable()
    {
        if (isVicBull)
        {
            Invoke("Destroy", 3f);
        }
        else if (isAngelBull)
        {
            Invoke("Destroy", 5f);
        }
        else if (isBow)
        {
            Destroy(this.gameObject, 3f);
        }
    }
    void Start()
    {
        if (isVicBull)
        {
            moveSpeed = vic_moveSpeed;
        }
        else if (isAngelBull)
        {
            moveSpeed = angel_moveSpeed;
        }
        else if (isBow)
        {
            moveSpeed = bow_moveSpeed;
        }
    }

    void Update()
    {
        transform.Translate(moveDirection * moveSpeed * Time.deltaTime);
    }
    public void SetMoveDirection(Vector2 _dir)
    {
        moveDirection = _dir;
    }
    public void Destroy()
    {
        if (isVicBull)
        {
            this.gameObject.SetActive(false);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
    void OnDisable()
    {
        CancelInvoke();
    }
}
