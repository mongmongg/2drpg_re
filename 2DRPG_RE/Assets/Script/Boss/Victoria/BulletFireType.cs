﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletFireType : MonoBehaviour
{
    public void FireCircleType(float startAngle, float endAngle, int bulletAmout)
    {
        float angleStep = (endAngle - startAngle) / bulletAmout;

        float angle = startAngle;

        for (int i = 0; i < bulletAmout + 1; i++)
        {
            float bullDirX = transform.position.x + Mathf.Sin((angle * Mathf.PI) / 180) * 360;
            float bullDiry = transform.position.x + Mathf.Cos((angle * Mathf.PI) / 180) * 360;

            Vector3 bullMoveVector = new Vector3(bullDirX, bullDiry, 0f);
            Vector2 bullDir = (bullMoveVector - transform.position).normalized;
            SpawnBullet(bullDir,1);
            angle += angleStep;

        }
    }
    public void FireToPlayerPosition(GameObject player, int bulletAmout)
    {
        for (int i = 0; i < bulletAmout + 1; i++)
        {
            float bullDirX = player.transform.position.x;
            float bullDiry = player.transform.position.y;

            Vector3 bullMoveVector = new Vector3(bullDirX, bullDiry, 0f);
            Vector2 bullDir = (bullMoveVector - transform.position).normalized;
            SpawnBullet(bullDir,0);
        }
    }
    void SpawnBullet(Vector2 _bullDir,int index )
    {
        GameObject bullet = BulletPool.bulletPoolInstance.GetBullet();        
        bullet.transform.position = transform.position;
        bullet.transform.rotation = transform.rotation;
        bullet.SetActive(true);
        bullet.GetComponent<Bullet>().SetMoveDirection(_bullDir);
        bullet.GetComponent<Bullet>().bulletType = (Bullet.BulletType)index;        
    }

}
