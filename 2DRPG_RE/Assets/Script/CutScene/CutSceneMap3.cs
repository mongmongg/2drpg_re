﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutSceneMap3 : MonoBehaviour
{
    public GameObject currentMap;
    public GameObject nextMap;
    public Transform changeToPos;

    // Start is called before the first frame update
    void Start()
    {
        nextMap.SetActive(false);
    }
    public void DisplayNextMap()
    {
        nextMap.SetActive(true);
        currentMap.SetActive(false);
        PlayerController.Instance.player.gameObject.transform.localPosition = new Vector3(changeToPos.position.x,
        changeToPos.position.y, 0);
        PlayerController.Instance.player.GetComponent<Movement>().enabled = false;
        Camera camera = Camera.main;
        camera.GetComponent<CameraMovement>().enabled = false;
        camera.transform.localPosition = new Vector3(-1.32f, 3f, -10);
    }
}
