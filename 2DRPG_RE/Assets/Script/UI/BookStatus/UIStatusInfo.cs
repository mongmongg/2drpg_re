﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
//using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIStatusInfo : MonoBehaviour
{
    public Button close_BTN;
    public Button home_BTN;
    public TextMeshProUGUI titleHeader;
    [Header("Player Status")]
    public Button status_BTN;
    public GameObject status_Panel;

    [Header("Inventory")]
    public Button inventory_BTN;
    public InventoryDisplay inventory_Panel;

    [Header("Quest")]
    public Button quest_BTN;
    public QuestDisplay quest_Panel;

    [Header("UnSelected")]
    public Button unselected_status;
    public Button unselected_inventory;
    public Button unselected_quest;

    private BookStatusCanvas bookStatusCanvas;
    // Start is called before the first frame update
    void Start()
    {
        close_BTN.onClick.AddListener(() => CloseUI());
        home_BTN.onClick.AddListener(() => HomeButton());
        inventory_BTN.onClick.AddListener(() => InventoryInterface());
        unselected_inventory.onClick.AddListener(() => InventoryInterface());
        status_BTN.onClick.AddListener(() => StatusInterface());
        unselected_status.onClick.AddListener(() => StatusInterface());
        quest_BTN.onClick.AddListener(() => QuestInterface());
        unselected_quest.onClick.AddListener(() => QuestInterface());
        SceneBookStatus.Instance.statusInfo_UI = this;
        //Debug.Log("UIStatus");
    }

    public void StatusInterface()
    {
        ClearPanel();
        unselected_inventory.gameObject.SetActive(true);
        unselected_quest.gameObject.SetActive(true);
        status_Panel.gameObject.SetActive(true);
        titleHeader.text = "Status";
    }
    public void InventoryInterface()
    {
        ClearPanel();
        unselected_status.gameObject.SetActive(true);
        unselected_quest.gameObject.SetActive(true);
        inventory_Panel.gameObject.SetActive(true);
        titleHeader.text = "Inventory";
    }
    public void QuestInterface()
    {
        ClearPanel();
        unselected_status.gameObject.SetActive(true);
        unselected_inventory.gameObject.SetActive(true);
        quest_Panel.gameObject.SetActive(true);
        titleHeader.text = "Quest";
    }
    void CloseUI()
    {
        this.gameObject.SetActive(false);
    }
    void HomeButton()
    {
        CloseUI();
        Instantiate(Resources.Load<AlertPopupPanel>("Prefabs/SceneBookStatus/Panel/AlertPopup"), transform.parent);
    }
    void ClearPanel()
    {
        status_Panel.gameObject.SetActive(false);
        inventory_Panel.gameObject.SetActive(false);
        quest_Panel.gameObject.SetActive(false);
        unselected_status.gameObject.SetActive(false);
        unselected_inventory.gameObject.SetActive(false);
        unselected_quest.gameObject.SetActive(false);
    }

}

