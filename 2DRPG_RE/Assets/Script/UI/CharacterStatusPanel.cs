﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterStatusPanel : MonoBehaviour
{
    public Image health;

    public void UpdateBar(float currentHealth, float minHealth, float maxHealth, bool show)
    {
        health.fillAmount = currentHealth / maxHealth;
    }
}
