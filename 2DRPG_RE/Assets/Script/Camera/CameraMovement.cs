﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[AddComponentMenu("2DRPG/Camera/CameraMovement")]
public class CameraMovement : MonoBehaviour
{
    public Transform target;
    public float smoothing = 0.1f;
    public Vector2 maxPosition;
    public Vector2 minPosition;

    [Header("Angelica Boss Map")]
    public bool angelicaBoss = false;

    void Start()
    {
        if (target == null)
        {
            if (angelicaBoss)
            {
                smoothing = 0.02f;
                target = Angelica.Instance.cameraAngelica;
                transform.SetParent(Angelica.Instance.cameraAngelica);
            }
            else
            {
                target = PlayerController.Instance.player.transform;
            }
        }
    }
    void LateUpdate()
    {
        if (angelicaBoss)
        {
            smoothing = 0.02f;
            target = Angelica.Instance.cameraAngelica;
            transform.SetParent(Angelica.Instance.cameraAngelica);
            Vector3 targetPosition = new Vector3(target.position.x,
                                                         target.position.y,
                                                         -10f);
            transform.position = Vector3.Lerp(transform.position,
                                                        targetPosition, smoothing);
        }
        else
        {
            target = PlayerController.Instance.player.transform;
            smoothing = 0.1f;
            transform.parent = null;
            if (target != null)
            {
                if (transform.position != target.position)
                {
                    Vector3 targetPosition = new Vector3(target.position.x,
                                                        target.position.y,
                                                        transform.position.z);
                    targetPosition.x = Mathf.Clamp(targetPosition.x, minPosition.x, maxPosition.x);
                    targetPosition.y = Mathf.Clamp(targetPosition.y, minPosition.y, maxPosition.y);
                    transform.position = Vector3.Lerp(transform.position,
                                                        targetPosition, smoothing);
                }
            }
        }
    }
}
