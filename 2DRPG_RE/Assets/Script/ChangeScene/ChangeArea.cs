﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeArea : MonoBehaviour
{
    public string area_name;

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            UIGamePlay.Instance.fadeImage.FadeOut();
            StartCoroutine(WaitToChangeScene());
        }
    }
    IEnumerator WaitToChangeScene()
    {
        yield return new WaitForSeconds(1f);
        LoadSceneManager.Instance.LoadToScene(area_name);
        Debug.Log("FadeOut");
    }
    // public void CheckIfScene()
    // {
    //     if (SceneManager.GetActiveScene().buildIndex == 3)
    //     {
    //         this.gameObject.SetActive(false);
    //     }
    // }
}
