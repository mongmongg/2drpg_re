﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum ItemType { General_Item, Quest_Item }
[System.Serializable]
public class RawDataItem
{
    public int IDItem;
    public string ItemName;
    public int ItemType;
}
[System.Serializable]
public class RawDataItemList
{
    public List<RawDataItem> _rawdataItemList;
}

