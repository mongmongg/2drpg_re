﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RawDataDialogue
{
    public int IDnpc;
    public string NpcName;
    public string CharacterName;
    public int NoDialogue;
    public string Dialogue;
    public int QuestID;
    public bool ShakeCam;
    public string Effect;
    public int LoadToScene;
    public string Scene;
}
[System.Serializable]
public class RawdataDialogueList
{
    public List<RawDataDialogue> _rawdataDialogue;
}

