﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// enum ItemType { General_Item, Quest_Item }
[System.Serializable]
public class RawDataQuest
{
    public int IDquest;
    public int No;
    public string NpcName;
    public string type;
    public string Title;
    public string Detail;
    public string NameItemQuest;
    public int QuantityItemQuest;
}
[System.Serializable]
public class RawDataQuestList
{
    public List<RawDataQuest> _rawdataQuest;
}

