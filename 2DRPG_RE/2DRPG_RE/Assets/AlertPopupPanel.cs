﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AlertPopupPanel : MonoBehaviour
{
    public Button yes_BTN;
    public Button no_BTN;
    void Start()
    {
        yes_BTN.onClick.AddListener(() => YesFunction());
        no_BTN.onClick.AddListener(() => NoFunction());
    }
    public virtual void YesFunction()
    {
        Destroy(this.gameObject);
        SceneManager.LoadScene(0);
    }
    void NoFunction()
    {
        Destroy(this.gameObject);
    }

}
