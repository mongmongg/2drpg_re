﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeImage : MonoBehaviour
{
    public Image image;
    Color fadeColor;
    private float targetAlpha;
    // Start is called before the first frame update
    void Awake()
    {
        image = GetComponent<Image>();
        fadeColor = image.color;
    }
    public void FadeIn()
    {
        StartCoroutine(FadeTo(0f, 1f));
    }
    public void FadeOut()
    {
        StartCoroutine(FadeTo(1f, 0.5f));
    }
    IEnumerator FadeTo(float aValue, float aTime)
    {
        float alpha = image.color.a;
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
        {
            Color newColor = new Color(0, 0, 0, Mathf.Lerp(alpha, aValue, t));
            image.color = newColor;
            yield return null;
        }
    }
}
