﻿using UnityEngine;

public enum Node
{
    StatusInfo,
    Inventory
}
public enum NodeType
{
    Scene, Panel
}

public class NodeData
{
    public Node node;
    public NodeType nodeType;
    public GameObject nodeObj;

    public NodeData(Node node, NodeType nodeType,GameObject nodeObj=null)
    {
        this.node = node;
        this.nodeType = nodeType;  
        this.nodeObj = nodeObj;

        if(nodeObj)
        this.nodeObj.gameObject.SetActive(false);
    }
}
