﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIGamePlay : SingletonBehaviour1<UIGamePlay>
{
    [Header("Interface Control Panel")]
    public Button character_Icon;
    public Button bag_Icon;
    [Header("Action Button")]
    public Button shield_BTN;
    public Button dash_BTN;
    public Button interaction_BTN;
    [Header("Action Control Panel")]
    public CanvasGroup action_ControlPanel;
    public CanvasGroup interactive_ControlPanel;
    private Scene scene;
    public UIStatusInfo uIStatusInfo;
    private BookStatusCanvas bookStatusCanvas;
    public SceneTitle sceneTitle;
    public FadeImage fadeImage;
    private void Awake()
    {
        Initialization();
        //Debug.Log("create canvas");
    }
    public void Initialization()
    {
        SetUIstatusInfo();

        //UIGamePlay.Instance.SwicthControl(false);
    }

    void SetUIstatusInfo()
    {
        sceneTitle = GetComponentInChildren<SceneTitle>();
        fadeImage = GetComponentInChildren<FadeImage>();
        if (SceneBookStatus.Instance.GetComponentInChildren<BookStatusCanvas>() != null)
        {
            if (uIStatusInfo == null)
            {
                Debug.Log("create canvas");
                uIStatusInfo = Instantiate(Resources.Load<UIStatusInfo>("Prefabs/SceneBookStatus/Panel/BookStatus"),
                SceneBookStatus.Instance.GetComponentInChildren<BookStatusCanvas>().content, false);
                SceneBookStatus.Instance.statusInfo_UI = uIStatusInfo;
                uIStatusInfo.gameObject.SetActive(false);
            }
            else
            {
                uIStatusInfo.gameObject.SetActive(true);
            }
        }
    }

    public void InterfaceStatusPanel(Node node)
    {
        SetUIstatusInfo();
        switch (node)
        {
            case Node.StatusInfo:
                if (uIStatusInfo != null)
                {
                    uIStatusInfo.StatusInterface();
                }
                break;

            case Node.Inventory:
                if (uIStatusInfo != null)
                {
                    uIStatusInfo.InventoryInterface();
                }
                break;
        }

    }
    void TestAssignActionToButton()
    {
        interaction_BTN.onClick.AddListener(() => Debug.Log("interaction"));
        dash_BTN.onClick.AddListener(() => Debug.Log("dash"));
    }
    public void ActionControl(bool isWork)
    {
        if (!isWork)
        {
            UIGamePlay.Instance.action_ControlPanel.alpha = 0;
            UIGamePlay.Instance.action_ControlPanel.blocksRaycasts = false;
        }
        else
        {
            UIGamePlay.Instance.action_ControlPanel.alpha = 1;
            UIGamePlay.Instance.action_ControlPanel.blocksRaycasts = true;
        }

    }
    public void InteractionControl(bool isWork)
    {
        if (!isWork)
        {
            UIGamePlay.Instance.interactive_ControlPanel.alpha = 0;
            UIGamePlay.Instance.interactive_ControlPanel.blocksRaycasts = false;
        }
        else
        {
            UIGamePlay.Instance.interactive_ControlPanel.alpha = 1;
            UIGamePlay.Instance.interactive_ControlPanel.blocksRaycasts = true;
        }

    }
    public void SwicthControl(bool actionIsWork)
    {
        if (actionIsWork)
        {
            ActionControl(false);
            InteractionControl(true);
        }
        else
        {
            ActionControl(true);
            InteractionControl(false);
        }
    }

    public BookStatusCanvas CreatePanel(NodeData nodeData)
    {
        return Instantiate(Resources.Load<BookStatusCanvas>("Prefabs/SceneBookStatus/Panel/" + nodeData.node.ToString()), bookStatusCanvas.content);
    }
}

