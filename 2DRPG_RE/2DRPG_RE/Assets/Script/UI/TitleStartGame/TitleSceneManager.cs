﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TitleSceneManager : MonoBehaviour
{
    public Button start_BTN;
    [Header("Continue Game")]
    public Button continue_BTN;
    public Button newGame_BTN;
    [Header("Setting")]
    public Button setting_BTN;
    public Button back_BTN;

    [Header("Interface")]
    public GameObject start_Interface;
    public GameObject setting_Interface;

    public int firstScene;
    private void Start()
    {
        Initialization();
    }
    void Initialization()
    {
        setting_BTN.onClick.AddListener(() => SettingInterface());
        start_BTN.onClick.AddListener(() => StartGameInterface());
        UIGamePlay.Instance.gameObject.SetActive(false);
        //GameController.Instance.inventoryManager.ClearInventoryManager();
        if (PlayerController.Instance != null)
        {
            Destroy(PlayerController.Instance.gameObject);
        }
    }
    void ClickNewGame()
    {
        UIGamePlay.Instance.gameObject.SetActive(true);
        SceneManager.LoadScene(firstScene);
        GameController.Instance.isNewGame = true;
        PlayerPrefs.SetString("SceneNumber", SceneManager.GetActiveScene().name);
    }
    void ClickContinue()
    {
        GameController.Instance.isNewGame = false;
        UIGamePlay.Instance.gameObject.SetActive(true);
        GameController.Instance.saveManager.LoadData();
    }
    void StartGameInterface()
    {
        ClearInterface();
        start_Interface.gameObject.SetActive(true);
        continue_BTN.onClick.AddListener(() => ClickContinue());
        newGame_BTN.onClick.AddListener(() => ClickNewGame());
    }
    void SettingInterface()
    {
        ClearInterface();
        setting_Interface.gameObject.SetActive(true);
    }
    void ClearInterface()
    {
        start_Interface.gameObject.SetActive(false);
        setting_Interface.gameObject.SetActive(false);
    }
}
