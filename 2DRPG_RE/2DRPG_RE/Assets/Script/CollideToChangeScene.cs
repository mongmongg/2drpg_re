﻿
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollideToChangeScene : MonoBehaviour
{
    public int loadtoscene = 4;
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag("Player"))
        {
            Debug.Log("Collide");
            GameController.Instance.saveManager.SaveData();
            SceneManager.LoadScene(loadtoscene);
        }
    }

}
