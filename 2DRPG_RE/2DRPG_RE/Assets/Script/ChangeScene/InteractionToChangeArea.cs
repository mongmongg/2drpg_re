﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InteractionToChangeArea : MonoBehaviour
{
    public string area_name;
    Collider2D m_collider;
    Character character;
    public bool conditionIsDone;
    private void OnTriggerEnter2D(Collider2D other)
    {
        m_collider = other;
        Interaction();
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        UIGamePlay.Instance.SwicthControl(false);
        UIGamePlay.Instance.interaction_BTN.onClick.RemoveAllListeners();
    }
    void Interaction()
    {
        if (CheckIfPlayer())
        {
            UIGamePlay.Instance.SwicthControl(true);
            if (conditionIsDone)
            {
                UIGamePlay.Instance.interaction_BTN.onClick.RemoveAllListeners();
                UIGamePlay.Instance.interaction_BTN.onClick.AddListener(() => ChangeArea());
            }
        }
    }
    bool CheckIfPlayer()
    {
        character = m_collider.GetComponent<Character>();
        if (character == null) { return false; }
        if (character.characterType == Character.CharacterTypes.Player) { return true; }
        return false;
    }
    void ChangeArea()
    {
        Debug.Log("Interaction Loadscene");
        UIGamePlay.Instance.SwicthControl(false);
        UIGamePlay.Instance.fadeImage.FadeOut();
        StartCoroutine(WaitToChangeScene());
    }
    IEnumerator WaitToChangeScene()
    {
        yield return new WaitForSeconds(1f);
        LoadSceneManager.Instance.LoadToScene(area_name);
        Debug.Log("FadeOut");
    }
}
