﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SceneTitle : MonoBehaviour
{
    public TextMeshProUGUI title_tmp;
    int sceneIndex;
    string sceneName;
    public void SetSceneName(int _index)
    {
        sceneIndex = _index;
        SceneName();
        title_tmp.text = sceneName;
        StartCoroutine(HideTitle());
    }
    public void ShowTitleQuest(string m_title)
    {
        title_tmp.text = m_title;
        StartCoroutine(HideTitle());
    }
    void SceneName()
    {
        switch (sceneIndex)
        {
            case 1:
                sceneName = "โรงอาหาร";
                break;
            case 2:
                sceneName = "";
                break;
            case 3:
                sceneName = "ห้องของ Alicia";
                break;
            case 4:
                sceneName = "ห้องพยาบาล";
                break;
            case 5:
                sceneName = "";
                break;
        }
    }
    IEnumerator HideTitle()
    {
        yield return new WaitForSeconds(2f);
        title_tmp.text = "";
    }
}
