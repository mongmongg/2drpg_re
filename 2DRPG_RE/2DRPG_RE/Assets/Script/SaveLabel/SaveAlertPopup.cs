﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveAlertPopup : AlertPopupPanel
{
    // Start is called before the first frame update
    public override void YesFunction()
    {        
        Destroy(this.gameObject);
        GameController.Instance.saveManager.SaveData();           
        Debug.Log("Save");        
    }
}
