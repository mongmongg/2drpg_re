﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveLabel : MonoBehaviour
{
    Character character;
    protected Collider2D m_collider;
    // Start is called before the first frame update
    void Start()
    {

    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        m_collider = collider;
        SaveGame();
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        UIGamePlay.Instance.SwicthControl(false);
    }
    void SaveGame()
    {
        if (CheckIsPlayer())
        {
            UIGamePlay.Instance.SwicthControl(true);
            UIGamePlay.Instance.interaction_BTN.onClick.RemoveAllListeners();
            UIGamePlay.Instance.interaction_BTN.onClick.AddListener(() => SavePanel());
        }
    }
    bool CheckIsPlayer()
    {
        character = m_collider.GetComponent<Character>();
        if (character == null) { return false; }
        if (character.characterType == Character.CharacterTypes.Player)
        {
            return true;
        }
        return false;
    }
    void SavePanel()
    {
        Transform uigameplay = UIGamePlay.Instance.transform;
        if (uigameplay != null)
        {
            Instantiate(Resources.Load<SaveAlertPopup>("Prefabs/SaveAlertPopup"), uigameplay);
        }
    }
}
