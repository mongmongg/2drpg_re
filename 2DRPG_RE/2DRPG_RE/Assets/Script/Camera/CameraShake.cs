﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("2DRPG/Camera/CameraShake")]
public class CameraShake : MonoBehaviour
{
    public void CheckShakeCam(RawDataDialogue rawDataDialogue)
    {
        if (rawDataDialogue.ShakeCam == true)
        {
            Debug.Log("Shake");
            StartCoroutine(Shake(1f, 1f));
        }
    }
    public IEnumerator Shake(float duration, float magnitude)
    {
        Vector3 originalPos = transform.localPosition;
        float elapse = 0.0f;
        while (elapse < duration)
        {
            float x = Random.Range(-1f, 1f) * magnitude;
            float y = Random.Range(-1f, 1f) * magnitude;

            transform.localPosition = new Vector3(x, y, originalPos.z);

            elapse += Time.deltaTime;

            yield return null;
        }
        transform.localPosition = originalPos;
    }
}
