﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class PickAbleItem : MonoBehaviour
{
    Character character;
    public ItemInfo itemInfo;

    [Header("Item Quantity")]
    public int Quantity = 1;
    protected Collider2D _itemCollider;
    protected Collider2D _pickingCollider;
    [Header("Pickable Item")]
    SpriteRenderer artwork;
    public bool disableObjectOnPick = false;

    InventoryManager inventoryManager;
    // Start is called before the first frame update
    protected virtual void Start()
    {
        _itemCollider = this.gameObject.GetComponent<Collider2D>();
        inventoryManager = GameController.Instance.inventoryManager;
        
        artwork = GetComponent<SpriteRenderer>();
        artwork.sprite = itemInfo.itemImage;
    }

    public virtual void OnTriggerEnter2D(Collider2D collider)
    {
        _pickingCollider = collider;
        PickItem();
    }
    protected virtual void PickItem()
    {
        if (CheckIfPickable())
        {
            inventoryManager.PickupManager(itemInfo, Quantity);
            if (disableObjectOnPick)
            {
                gameObject.SetActive(false);
            }
        }
    }

    protected virtual bool CheckIfPickable()
    {
        character = _pickingCollider.GetComponent<Character>();
        if (character == null) { return false; }

        if (character.characterType == Character.CharacterTypes.Player)
        {
            return true;
        }
        return false;
    }
}
