﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class CreateItemInfoFromData : MonoBehaviour
{
    Dictionary<int, RawDataItem> dict_ItemData;
    // Start is called before the first frame update
    void Start()
    {
        dict_ItemData = ConvertData.Instance.dict_ItemDataJSON;
        Create();        
    }

    void Create()
    {
        foreach (RawDataItem a in dict_ItemData.Values)
        {
            if (CheckResource(a.ItemName))
            {
                ItemInfo newitem = new ItemInfo(a.IDItem, a.ItemName, a.ItemType);
                AssetDatabase.CreateAsset(newitem, "Assets/Resources/Item/" + a.ItemName + ".asset");
            }
            else
            {
                ItemInfo item = Resources.Load<ItemInfo>("Item/" + a.ItemName);                
                item.itemID = a.IDItem;
                item.itemname  = a.ItemName;
                item.itemType = (ItemInfo.ItemTypes)a.ItemType;
                //item.itemType =(ItemType)a.ItemType;                
            }
        }
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

    }
    bool CheckResource(string _name)
    {
        if (Resources.Load<ItemInfo>("Item/" + _name) == null)
        {
            return true;
        }
        return false;
    }
}
