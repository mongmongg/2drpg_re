﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Item/item")]
public class ItemInfo : ScriptableObject
{
    public enum ItemTypes { General_Item, Quest_Item }
    [Header("ID and Target")]
    public string itemname;
    public int itemID;
    public int quantity = 1;
    //public int itemAmount;
    [Header("Methods")]
    public ItemTypes itemType;
    // protected bool Usable;
    // public virtual bool IsUsable { get { return Usable; } }
    [Header("Image")]
    public Sprite itemImage;

    //Use Item Button
    public virtual bool Use()
    {
        return true;
    }
    public ItemInfo(int id,string _name,int _type)
    {
        itemID = id;
        itemname = _name;
        itemType = (ItemTypes)_type;   
        itemImage =Resources.Load<Sprite>("Item/Sprite/" + _name);
    }
}
