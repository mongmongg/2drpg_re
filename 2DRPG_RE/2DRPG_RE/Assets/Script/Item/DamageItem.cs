﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageItem : MonoBehaviour
{
    Character character;
    public int damage;
    public bool canAttack;
    protected Collider2D _itemCollider;
    protected Collider2D _pickingCollider;
    public bool disableObjectOnPick = false;
    public bool destroyObject = false;
    private void OnEnable()
    {
        canAttack = true;
    }
    private void OnDisable()
    {
        canAttack = false;
    }
    public virtual void OnTriggerEnter2D(Collider2D collider)
    {
        if (canAttack)
        {
            _pickingCollider = collider;
            OnCollide();
        }
    }
    protected virtual void OnCollide()
    {
        if (CheckIfPickable())
        {
            Health characterHealth = _pickingCollider.GetComponent<Health>();
            characterHealth.TakeDamage(damage);
            if (disableObjectOnPick)
            {
                gameObject.SetActive(false);
            }
            if (destroyObject)
            {
                Destroy(gameObject);
            }
        }
        if (CheckIfCollideTheWall())
        {
            Destroy(gameObject);
        }
    }
    protected virtual bool CheckIfPickable()
    {
        character = _pickingCollider.GetComponent<Character>();
        if (character == null) { return false; }

        if (character.characterType == Character.CharacterTypes.Player)
        {
            return true;
        }
        return false;
    }
    bool CheckIfCollideTheWall()
    {
        if (_pickingCollider.tag == "Wall") { return true; }
        return false;
    }

}
