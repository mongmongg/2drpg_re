﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneBookStatus : SingletonBehaviour<SceneBookStatus>
{
    public UIStatusInfo statusInfo_UI;    
    BookStatusCanvas bookStatusCanvas;
    public GameObject dialogueCanvas;
    //public DialogueCanvas dialogueCanvas;
    void Awake()
    {
        //statusInfo_UI = GetComponentInChildren<UIStatusInfo>();
        bookStatusCanvas = Instantiate(Resources.Load<BookStatusCanvas>("Prefabs/SceneBookStatus/Canvas"), transform);
        dialogueCanvas = Instantiate(Resources.Load<GameObject>("Dialogue/DialogueCanvas"),transform);
    }
}
