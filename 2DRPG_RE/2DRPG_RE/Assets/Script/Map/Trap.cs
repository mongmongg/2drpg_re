﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : MonoBehaviour
{
    [Header("FloorTrap")]
    public bool floor;
    public float floor_repeatRate;
    public Collider2D trapCollider;
    [Header("Wall Trap")]
    public bool wall;
    public GameObject bullet;
    public float wall_repeatRate;
    Animator animator;
    SpriteRenderer sprite;
    float randNumber;
    void Start()
    {
        RandomFloat(3f);
        if (floor)
        {
            animator = GetComponent<Animator>();
            sprite = GetComponent<SpriteRenderer>();
            trapCollider = GetComponent<Collider2D>();
            InvokeRepeating("FloorTrap", 0f, randNumber);
        }
        else if (wall)
        {
            InvokeRepeating("WallTrap", 0, randNumber);
        }
    }
    void FloorTrap()
    {
        trapCollider.enabled = true;
        animator.SetBool("Attack", true);
    }

    void WallTrap()
    {
        Debug.Log("WallTrap");
        GameObject test = Instantiate(bullet, this.transform);
        test.transform.localPosition = Vector2.zero;
        test.GetComponent<Bullet>().enabled = true;
        test.GetComponent<Bullet>().SetMoveDirection(Vector2.down);
    }
    void RandomFloat(float maxFloat)
    {
        randNumber = Random.Range(1f, maxFloat);
        floor_repeatRate = randNumber;
        wall_repeatRate = randNumber;
    }


}
