﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class AngelicaMovement : MonoBehaviour
{

    public float MovementSpeed;

    void Update()
    {
        Movement();
    }

    void Movement()
    {
        transform.Translate(Vector2.right * MovementSpeed * Time.deltaTime);
    }
}
