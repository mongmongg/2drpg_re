﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AngelicaTrigger : MonoBehaviour
{
    Collider2D _collider;
    AngelicaMovement angelicaMovement;

    private void OnTriggerEnter2D(Collider2D other)
    {
        _collider = other;
        ChangeSkill();
    }
    void ChangeSkill()
    {
        // if (CheckIfAngelica())
        // {
        //     angelicaMovement.skillPattern = AngelicaMovement.SkillPattern.Line;
        // }
    }
    bool CheckIfAngelica()
    {
        angelicaMovement = _collider.GetComponent<AngelicaMovement>();
        if (angelicaMovement == null) { return false; }
        return true;
    }
}
