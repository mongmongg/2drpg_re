﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AngelicaSkillLine : MonoBehaviour
{
    public enum SkillPattern { RandomPoint, Line }
    public SkillPattern skillPattern;
    [Header("Skill 1")]
    public GameObject spawnPointRand;
    public List<AngelicaBulletSpawnPoint> spawnPointsSkill_rand;

    private List<AngelicaBulletSpawnPoint> spawnPointsSkillAll;

    [Header("Skill 2")]
    public GameObject spawnPointLine;
    public List<AngelicaBulletSpawnPoint> spawnPointsSkill_line;
    float randNumber;
    // Start is called before the first frame update
    void Start()
    {


    }
    void UseSkill()
    {
        if (skillPattern == SkillPattern.RandomPoint)
        {

        }
        else if (skillPattern == SkillPattern.Line)
        {

        }
    }
    void FireRandomPoint()
    {
        StartCoroutine(RandomSkill(5f));
    }
    void FireLine()
    {
        StartCoroutine(RandomSkill(5f));
        //InvokeRepeating("RandomSkill", 5f, 5f);
    }
    IEnumerator RandomSkill(float m_time)
    {
        yield return new WaitForSeconds(m_time);
        randNumber = Random.Range(0f, 2f);
        if (0f < randNumber && randNumber < 1.0f)
        {
            skillPattern = SkillPattern.RandomPoint;
            UseSkill();
        }
        else if (1.0f <= randNumber && randNumber < 2.0f)
        {
            skillPattern = SkillPattern.Line;
            UseSkill();
        }
    }
}
