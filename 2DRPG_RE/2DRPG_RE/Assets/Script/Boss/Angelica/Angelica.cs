﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Angelica : SingletonBehaviour1<Angelica>
{
    public Transform cameraAngelica;
    [HideInInspector]
    public AngelicaMovement angelicaMovement;
    public GameObject wall;
    Collider2D playerCollider;
    Character character;
    // Start is called before the first frame update
    void Start()
    {
        angelicaMovement = GetComponent<AngelicaMovement>();
        wall.SetActive(false);
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (this.gameObject.tag != "Wall")
        {
            playerCollider = other;
            PlayerFail();
        }
    }
    void PlayerFail()
    {
        if (CheckIfPlayer())
        {
            Debug.Log("Player Dead");
            angelicaMovement.MovementSpeed = 0f;
            PlayerController.Instance.player.GetComponent<Health>().PlayerDead();
        }
    }
    bool CheckIfPlayer()
    {
        character = playerCollider.GetComponent<Character>();
        if (character == null) { return false; }
        if (character.characterType == Character.CharacterTypes.Player) { return true; }
        return false;
    }
    void Update()
    {

    }
}
