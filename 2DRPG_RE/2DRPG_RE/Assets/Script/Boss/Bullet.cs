﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Vector2 moveDirection;
    private float moveSpeed;
    [Header("Victoria")]
    public bool isVicBull;
    public float vic_moveSpeed = 5f;
    [Header("Angelica")]
    public bool isAngelBull;
    public float angel_moveSpeed = 7f;

    private void OnEnable()
    {
        if (isVicBull)
        {
            Invoke("Destroy", 3f);
        }
        else if (isAngelBull)
        {
            Invoke("Destroy", 5f);
        }
    }
    void Start()
    {
        if (isVicBull)
        {
            moveSpeed = vic_moveSpeed;
        }
        else if (isAngelBull)
        {
            moveSpeed = angel_moveSpeed;
        }
    }

    void Update()
    {
        transform.Translate(moveDirection * moveSpeed * Time.deltaTime);
    }
    public void SetMoveDirection(Vector2 _dir)
    {
        moveDirection = _dir;
    }
    public void Destroy()
    {
        Destroy(this.gameObject);
        //gameObject.SetActive(false);
    }

    void OnDisable()
    {
        CancelInvoke();
    }
}
