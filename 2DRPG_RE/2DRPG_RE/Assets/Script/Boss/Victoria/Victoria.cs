﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class Victoria : SingletonBehaviour1<Victoria>
{
    public AIPath aIPath;
    [Header("HP")]
    public int currentHP;
    [SerializeField]
    protected int initHP = 7;
    protected Animator animator;
    VictoriaSkillPattern victoriaSkill;
    public int countCrystalDestroy = 0;

    void Awake()
    {
        Initilization();
    }
    protected virtual void Initilization()
    {
        currentHP = initHP;
        victoriaSkill = GetComponent<VictoriaSkillPattern>();
        aIPath = GetComponentInParent<AIPath>();
        animator = GetComponent<Animator>();
        PlayerPrefs.SetInt("Victoria", 0);
    }
    private void Update()
    {
        if (victoriaSkill.skillPattern != VictoriaSkillPattern.SkillPattern.Shield)
        {
            if (aIPath.desiredVelocity.x >= 0.01f)
            {
                transform.localScale = new Vector3(1f, 1f, 1f);
            }
            else if (aIPath.desiredVelocity.x <= -0.01f)
            {
                transform.localScale = new Vector3(-1f, 1f, 1f);
            }
        }

        if (IsDead())
        {
            animator.SetBool("isDead", true);
            aIPath.canMove = false;
            victoriaSkill.CancelInvoke();
            PlayerPrefs.SetInt("Victoria", 1);
        }
    }
    public bool IsDead()
    {
        if (currentHP <= 0) { return true; }
        return false;
    }
}
