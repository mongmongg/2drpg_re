﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class VictoriaSkillPattern : MonoBehaviour
{
    public enum SkillPattern { ShootToPlayer, Circle, Shield }
    public SkillPattern skillPattern;
    public SkillPattern currentSkill;
    BulletFireType bulletFireType;
    [SerializeField]
    private int bulletAmout = 8;
    [SerializeField]
    private float startAngle = 0f, endAngle = 360f;
    private Vector2 bulletMoveDirection;
    public GameObject player;
    public GameObject victoria_shield;
    float randNum;
    AIPath aIPath;
    Animator animator;

    private void Start()
    {
        Initilization();
    }
    void Initilization()
    {
        aIPath = GetComponentInParent<AIPath>();
        animator = GetComponent<Animator>();

        bulletFireType = GetComponent<BulletFireType>();
        player = PlayerController.Instance.player;
        victoria_shield = GetComponentInChildren<DamageItem>().gameObject;
        victoria_shield.SetActive(false);
        InvokeRepeating("RandomSkill", 0f, 5f);
    }

    bool ChangeSkill()
    {
        if (skillPattern != currentSkill)
        {
            return true;
        }
        else return false;
    }
    void UseSkill()
    {
        currentSkill = skillPattern;
        if (skillPattern == SkillPattern.Circle)
        {
            animator.SetTrigger("Skill2");
            CancelInvoke();
            InvokeRepeating("FireCircleType", 0.5f, 2f);
        }
        else if (skillPattern == SkillPattern.ShootToPlayer)
        {
            animator.SetTrigger("Skill1");
            CancelInvoke();
            InvokeRepeating("FireToPlayerPosition", 0.5f, 2f);
        }
        else
        {
            animator.SetTrigger("Skill3");
            CancelInvoke();
            victoria_shield.SetActive(true);
            InvokeRepeating("Shield", 0.8f, 4f);
        }
    }
    void FireCircleType()
    {
        InvokeRepeating("RandomSkill", 5f, 5f);
        aIPath.canMove = true;
        bulletFireType.FireCircleType(startAngle, endAngle, bulletAmout);
        victoria_shield.SetActive(false);
    }
    void FireToPlayerPosition()
    {
        InvokeRepeating("RandomSkill", 5f, 5f);
        aIPath.canMove = true;
        bulletFireType.FireToPlayerPosition(player, 0);
        victoria_shield.SetActive(false);
    }
    void Shield()
    {
        InvokeRepeating("RandomSkill", 5f, 7f);
        aIPath.canMove = false;
        Vector3 dashPosition = player.transform.position;
        Vector3 victoria = this.transform.position;
        float num = dashPosition.x - victoria.x;
        LeanTween.move(aIPath.gameObject, dashPosition, 0.3f);
        if (num >= 0f)
        {
            this.transform.localScale = new Vector3(1f, 1f, 1f);
        }
        else if (num < 0f)
        {
            this.transform.localScale = new Vector3(-1f, 1f, 1f);
        }
    }

    void RandomSkill()
    {
        randNum = Random.Range(0f, 3f);
        Debug.Log(randNum);
        if (0f < randNum && randNum < 0.8f)
        {
            Debug.Log("Skill1");
            skillPattern = SkillPattern.ShootToPlayer;
            UseSkill();
        }
        else if (0.8f <= randNum && randNum < 2.0f)
        {
            Debug.Log("Skill2");
            skillPattern = SkillPattern.Circle;
            UseSkill();
        }
        else
        {
            skillPattern = SkillPattern.Shield;
            Debug.Log("Skill3");
            UseSkill();
        }
    }
}

