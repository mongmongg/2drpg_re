﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBoss : MonoBehaviour
{
    public bool bossVictoria;
    public bool deletePlayerPefs;
    public Transform spwanPos;
    // Start is called before the first frame update
    void Start()
    {
        if (bossVictoria)
        {
            SpawnBossVictoria();
        }
        if (deletePlayerPefs)
        {
            PlayerPrefs.DeleteKey("Victoria");
        }
    }
    void SpawnBossVictoria()
    {
        if (PlayerPrefs.GetInt("Victoria") == 0)
        {
            VictoriaSkillPattern victoria = Instantiate(Resources.Load<VictoriaSkillPattern>("Prefabs/Character/Boss/Victoria/Victoria"), spwanPos);
            victoria.gameObject.transform.localPosition = transform.position;
        }
    }
    // Update is called once per frame
    void Update()
    {

    }
}
