﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour
{
    public ItemInfo itemInfo;
    public Image artwork;
    public TextMeshProUGUI amoutText;
    public int amount;
    Button slot_BTN;

    void Awake()
    {
        slot_BTN = GetComponentInChildren<Button>();
        slot_BTN.onClick.AddListener(() => UseButtonOnClick());
        artwork = GetComponentInChildren<Image>();
        amoutText = GetComponentInChildren<TextMeshProUGUI>();
    }

    public void Initialization(ItemInfo m_itemInfo, int quantity)
    {
        itemInfo = m_itemInfo;
        artwork.sprite = itemInfo.itemImage;
        amount = quantity;
        UpdateUISlot();
    }
    public void UpdateUISlot()
    {
        amoutText.text = amount.ToString();
    }
    void UseButtonOnClick()
    {
        CheckIfUsable();
        UIGamePlay.Instance.uIStatusInfo.inventory_Panel.use_BTN.onClick.RemoveAllListeners();
        UIGamePlay.Instance.uIStatusInfo.inventory_Panel.use_BTN.onClick.AddListener(() => UseItemInInventory());
    }
    public void UseItemInInventory()
    {
        amount -= 1;
        UpdateUISlot();
        RemoveItem();

    }
    public void UseItem(int quantity)
    {
        if (CheckIfUsable())
        {
            amount -= quantity;
            UpdateUISlot();
            RemoveItem();
        }
    }
    protected bool CheckIfUsable()
    {
        if (itemInfo.itemType != ItemInfo.ItemTypes.Quest_Item && amount > 0)
        {
            UIGamePlay.Instance.uIStatusInfo.inventory_Panel.use_BTN.interactable = true;
            return true;
        }
        UIGamePlay.Instance.uIStatusInfo.inventory_Panel.use_BTN.interactable = false;
        //if (itemInfo.itemType == ItemInfo.ItemType.Quest_Item) { return false; }
        return false;
    }
    bool RemoveItem()
    {
        if (amount <= 0)
        {
            GameController.Instance.inventoryManager.RemoveItemManager(itemInfo);
            return true;
        }
        return false;
    }

}
