﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
[AddComponentMenu("2DRPG/Manager/DialogueManager")]
public class DialogueManager : MonoBehaviour
{
    private Queue<RawDataDialogue> sentences;
    private List<string> list_sentence;
    RawDataDialogue rawDataDialogue;
    //[HideInInspector]
    public List<RawDataDialogue> list_rawDataDialogues;
    //public List<string> list_npc;
    NpcController[] npcControllers;
    public GameObject[] npcNames;
    public int currentNoDialogue = 2;
    NpcController currentNpc;
    DialogueTextBox dialogueTextBox;

    public virtual void Initilization(Scene m_scene)
    {
        sentences = new Queue<RawDataDialogue>();
        ShowData(m_scene);
        npcControllers = (NpcController[])GameObject.FindObjectsOfType(typeof(NpcController));
        FindNpcInThisScene();
        SetDialogToNPC();
    }
    void ShowData(Scene m_scene)
    {
        list_rawDataDialogues = new List<RawDataDialogue>();
        foreach (RawDataDialogue m_rawData in ConvertData.Instance.dict_DialogueDataJSON.Values)
        {
            //rawDataDialogues.Add(m_rawData);
            if (m_rawData.Scene == m_scene.name)
            {
                list_rawDataDialogues.Add(m_rawData);
            }
        }
    }
    void FindNpcInThisScene()
    {
        npcNames = new GameObject[npcControllers.Length];
        for (var i = 0; i < npcControllers.Length; i++)
        {
            npcNames[i] = npcControllers[i].gameObject;
        }
    }
    void SetDialogToNPC()
    {
        foreach (RawDataDialogue sentence in list_rawDataDialogues)
        {
            if (sentence.NoDialogue == currentNoDialogue)
            {
                Debug.Log(sentence.NpcName);
                SetSign(sentence);
                break;
            }
        }
    }
    void SetSign(RawDataDialogue sentence)
    {
        foreach (GameObject a in npcNames)
        {
            if (sentence.NpcName == a.name)
            {
                currentNpc = a.GetComponent<NpcController>();
                currentNpc.ShowDialogSign();
                currentNpc.isCurrentDialogue = true;
                break;
            }
        }
    }
    public int numQuest;
    public void StartDialogue()
    {
        Debug.Log("Interact Click");
        sentences.Clear();

        foreach (RawDataDialogue sentence in list_rawDataDialogues)
        {
            if (sentence.NoDialogue == currentNoDialogue)
            {
                sentences.Enqueue(sentence);
                numQuest = sentence.QuestID;
            }
        }
        Debug.Log(numQuest);
        GameController.Instance.questManager.FindQuestInDATA(numQuest);
        DisplayNextSentence();
    }
    public void DisplayNextSentence()
    {
        UIGamePlay.Instance.interaction_BTN.onClick.RemoveAllListeners();
        UIGamePlay.Instance.interaction_BTN.onClick.AddListener(() => DisplayNextSentence());
        if (sentences.Count == 0)
        {
            EndDialogue();
            return;
        }
        RawDataDialogue sentence = sentences.Dequeue();
        GameController.Instance.cameraShake.CheckShakeCam(sentence);
        CreateDialogueBox(sentence);
        GameController.Instance.dialogueEffect.CheckEffect(sentence);
    }
    public void EndDialogue()
    {
        GameController.Instance.questManager.AddQuest();
        UIGamePlay.Instance.SwicthControl(false);
        currentNpc.DestroyDialogSign();
        DestroyDialogBox();
        NextNPCDialogue();
    }
    public void NextNPCDialogue()
    {
        currentNoDialogue++;
        SetDialogToNPC();
    }

    void CreateDialogueBox(RawDataDialogue dialogueText)
    {
        if (dialogueText.CharacterName != "NoOne")
        {
            if (dialogueTextBox == null)
            {
                dialogueTextBox = Instantiate(Resources.Load<DialogueTextBox>("Dialogue/Dialogue TextBox"), currentNpc.dialogSign_Position);
                dialogueTextBox.transform.SetParent(SceneBookStatus.Instance.dialogueCanvas.transform, false);
                dialogueTextBox.gameObject.transform.position = currentNpc.dialogSign_Position.position;
                dialogueTextBox.SetDialogue(dialogueText);
            }
            else
            {
                dialogueTextBox.SetDialogue(dialogueText);
            }
        }
        else { UIGamePlay.Instance.sceneTitle.ShowTitleQuest(dialogueText.Dialogue); }
    }
    public void DestroyDialogBox()
    {
        if (dialogueTextBox != null)
        {
            Destroy(dialogueTextBox.gameObject);
        }
    }
}
