﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
[AddComponentMenu("2DRPG/Manager/GameController")]
public class GameController : SingletonBehaviour1<GameController>
{
    [Header("Inventory")]
    public InventoryManager inventoryManager;
    public Inventory inventory;

    [Header("Dialogue")]
    public DialogueManager dialogueManager;
    public DialogueEffect dialogueEffect;

    [Header("Quest")]
    public QuestManager questManager;

    [Header("GameSave")]
    public GameSaveManager saveManager;

    [HideInInspector]
    public Camera mainCam;
    [Header("Camera")]
    public CameraMovement cameraMovement;
    public CameraShake cameraShake;
    //[Header("Player")]
    public PlayerController playerController;
    public bool isNewGame = false;
    //SceneBookStatus sceneBookStatus;
    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }
    void OnSceneLoaded(Scene m_scene, LoadSceneMode m_mode)
    {
        string previousScene = PlayerPrefs.GetString("SceneNumber");        
        Initilization(m_scene);
        NewGame();
        UIGamePlay.Instance.sceneTitle.SetSceneName(m_scene.buildIndex);
        LoadSceneManager.Instance.SpawnPlayerToPosition(previousScene);
    }
    private void Awake()
    {
        GetComponentGameobject();
        if (SceneManager.GetActiveScene().buildIndex != 0)
        {
            mainCam = Camera.main;
            cameraMovement = mainCam.GetComponent<CameraMovement>();
            cameraShake = mainCam.GetComponent<CameraShake>();
            if (PlayerController.Instance != null)
            {
                playerController = PlayerController.Instance;
            }
        }
        //OnSceneLoaded(SceneManager.GetActiveScene(), LoadSceneMode.Single);
    }
    void GetComponentGameobject()
    {
        inventory = GetComponent<Inventory>();
        inventoryManager = GetComponent<InventoryManager>();
        dialogueManager = GetComponent<DialogueManager>();
        dialogueEffect = GetComponent<DialogueEffect>();
        questManager = GetComponent<QuestManager>();
        saveManager = GetComponent<GameSaveManager>();
        mainCam = Camera.main;
        cameraMovement = mainCam.GetComponent<CameraMovement>();
        cameraShake = mainCam.GetComponent<CameraShake>();
    }
    public virtual void Initilization(Scene m_scene)
    {
        UIGamePlay.Instance.fadeImage.FadeIn();
        mainCam = Camera.main;
        cameraMovement = mainCam.GetComponent<CameraMovement>();
        cameraShake = mainCam.GetComponent<CameraShake>();
        dialogueManager.Initilization(m_scene);
        LoadSceneManager.Instance.LoadPlayerPosition();
        questManager.UpdateSceneQuest();
    }
    void NewGame()
    {
        if (isNewGame)
        {
            dialogueManager.currentNoDialogue = 1;
            questManager.currentQuest = 0;
            PlayerController.Instance.player.GetComponent<Health>().SetHealth(80);
        }
    }
}
