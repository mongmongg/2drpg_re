﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    [Header("InventorySlot")]
    InventorySlot inventorySlot;
    public Transform slotPosition;
    public Dictionary<int, InventorySlot> dict_inventorySlot;

    void Start()
    {
        dict_inventorySlot = new Dictionary<int, InventorySlot>();
        slotPosition = SceneBookStatus.Instance.statusInfo_UI.inventory_Panel.displayItem;
    }
    public void PickItem(ItemInfo item, int quantity)
    {
        if (dict_inventorySlot.ContainsKey(item.itemID))
        {
            dict_inventorySlot[item.itemID].amount += quantity;
            dict_inventorySlot[item.itemID].UpdateUISlot();
        }
        else
        {
            Add(item, quantity);
        }
    }
    public void Add(ItemInfo item, int quantity)
    {
        InventorySlot newSlot = Instantiate(Resources.Load<InventorySlot>("Prefabs/Inventory Slot"), slotPosition.transform);
        dict_inventorySlot.Add(item.itemID, newSlot);
        dict_inventorySlot[item.itemID].Initialization(item, quantity);
    }
    public void RemoveFromInventory(ItemInfo item)
    {
        Destroy(dict_inventorySlot[item.itemID].gameObject);
        dict_inventorySlot.Remove(item.itemID);
    }
    public void RemoveAllItem()
    {
        foreach (Transform a in slotPosition.transform)
        {
            Destroy(a.gameObject);
        }
        dict_inventorySlot.Clear();
    }
}
