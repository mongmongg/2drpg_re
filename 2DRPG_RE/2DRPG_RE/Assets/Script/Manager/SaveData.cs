﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveData
{
    [Header("Inventory")]
    public List<string> itemDataName;
    public List<int> itemDataQuantity;

    [Header("Player")]
    public int p_HP;
    //public float playerPositionX;
    //public float playerPositionY;
    [Header("Dialogue")]
    public int currentDialogue;
    [Header("Quest")]
    public int currentQuest;
    [Header("Scene")]
    public int sceneIndex;
}
