﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour
{
    public void PickupManager(ItemInfo m_item, int quantity)
    {
        //questManager_Fix.UpdateQuest();
        GameController.Instance.inventory.PickItem(m_item, quantity);
        GameController.Instance.questManager.UpdateItemQuest();
        //inventory.PickItem(_item, quantity);
    }
    public void RemoveItemManager(ItemInfo m_item)
    {
        GameController.Instance.inventory.RemoveFromInventory(m_item);
    }
    public void ClearInventoryManager()
    {
        GameController.Instance.inventory.RemoveAllItem();
    }

}
