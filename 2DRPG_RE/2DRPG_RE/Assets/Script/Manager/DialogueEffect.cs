﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DialogueEffect : MonoBehaviour
{
    Button m_btn;
    public void CheckEffect(RawDataDialogue rawDataDialogue)
    {
        if (rawDataDialogue.Effect == "LoadScene")
        {
            m_btn = UIGamePlay.Instance.interactive_ControlPanel.GetComponentInChildren<Button>();
            m_btn.GetComponent<Button>().enabled = false;
            StartCoroutine(WaitToLoadScene(rawDataDialogue));
        }
    }
    IEnumerator WaitToLoadScene(RawDataDialogue rawDataDialogue)
    {
        yield return new WaitForSeconds(3f);
        GameController.Instance.dialogueManager.EndDialogue();
        SceneManager.LoadScene(rawDataDialogue.LoadToScene);
        m_btn.GetComponent<Button>().enabled = true;
    }
}
