﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
[AddComponentMenu("2DRPG/Manager/QuestManager")]
public class QuestManager : MonoBehaviour
{
    public List<RawDataQuest> rawDataQuest;
    public List<int> idCompleteQuest;
    List<QuestItemSlot> list_questItem;
    public int currentQuest;
    private void Start()
    {
        idCompleteQuest = new List<int>();        
        //ShowData();
        //rawDataQuest = new RawDataQuest();
    }
    void ShowData()
    {
        foreach (RawDataQuest m_rawdata in ConvertData.Instance.dict_QuestDataJSON.Values)
        {
            rawDataQuest.Add(m_rawdata);
        }
    }
    void TestLoadQuest()
    {
        FindQuestInDATA(currentQuest);
        AddQuest();
    }
    public void FindQuestInDATA(int noQuest)
    {
        currentQuest = noQuest;
        if (noQuest != 0)
        {
            rawDataQuest = new List<RawDataQuest>();
            foreach (RawDataQuest m_rawdata in ConvertData.Instance.dict_QuestDataJSON.Values)
            {
                if (m_rawdata.No == noQuest)
                {
                    rawDataQuest.Add(m_rawdata);
                }
            }
        }

    }
    public void AddQuest()
    {
        if (CheckIfHaveQuest())
        {
            list_questItem = new List<QuestItemSlot>();
            QuestDisplay questDisplay = SceneBookStatus.Instance.statusInfo_UI.quest_Panel;
            questDisplay.questDisplay.SetActive(true);
            questDisplay.titleQuest.text = rawDataQuest[0].Title;
            UIGamePlay.Instance.sceneTitle.ShowTitleQuest(questDisplay.titleQuest.text);
            questDisplay.detailQuest.text = rawDataQuest[0].Detail;
            if (rawDataQuest[0].type == "Item")
            {
                foreach (RawDataQuest a in rawDataQuest)
                {
                    QuestItemSlot newSlot = Instantiate(Resources.Load<QuestItemSlot>("Prefabs/ItemQuestSlot"), questDisplay.questItemslot_position);
                    newSlot.Initialization(a.NameItemQuest, a.QuantityItemQuest);
                    list_questItem.Add(newSlot);
                }
                UpdateItemQuest();
            }
            else if (rawDataQuest[0].type == "Scene")
            {
                UpdateSceneQuest();
            }
        }
    }
    bool CheckIfHaveQuest()
    {
        if (rawDataQuest.Count > 0)
        {
            if (rawDataQuest[0].IDquest != 0)
            {
                return true;
            }
        }
        return false;
    }
    bool IsSceneQuest()
    {
        if (rawDataQuest.Count != 0)
        {
            if (rawDataQuest[0].type == "Scene")
            {
                return true;
            }
        }
        return false;
    }
    public void UpdateSceneQuest()
    {
        if (IsSceneQuest())
        {
            int index = int.Parse(rawDataQuest[0].NameItemQuest);
            if (SceneManager.GetActiveScene().buildIndex == index)
            {
                RemoveQuest();
            }
            else { Debug.Log("Not Yet"); }
        }
    }
    public void UpdateItemQuest()
    {
        Debug.Log("Update");
        if (list_questItem != null)
        {
            foreach (QuestItemSlot a in list_questItem)
            {
                a.UpdateItemSlot();
            }
            //IsQuestComplete();
            RemoveQuest();
        }
    }
    public bool IsQuestComplete()
    {
        int count = 0;
        foreach (QuestItemSlot a in list_questItem)
        {
            if (a.isComplete)
            {
                count++;
            }
        }
        if (count == list_questItem.Count)
        {
            return true;
        }
        return false;
    }
    void RemoveQuest()
    {
        if (IsQuestComplete())
        {
            QuestDisplay questDisplay = SceneBookStatus.Instance.statusInfo_UI.quest_Panel;
            foreach (Transform a in questDisplay.questItemslot_position)
            {
                Destroy(a.gameObject);
            }
            idCompleteQuest.Add(rawDataQuest[0].IDquest);
            rawDataQuest.Clear();
        }
    }
}
