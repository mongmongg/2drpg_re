﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestBossDead : MonoBehaviour
{
    Collider2D _collider;
    Character character;
    private void OnTriggerEnter2D(Collider2D other)
    {
        _collider = other;
        if (CheckIfPlayer())
        {
            Victoria victoria = FindObjectOfType<Victoria>();
            //victoria.currentHP = 0;
        }
    }
    bool CheckIfPlayer()
    {
        character = _collider.GetComponent<Character>();
        if (character == null) { return false; }
        if (character.characterType == Character.CharacterTypes.Player)
        {
            return true;
        }
        return false;
    }
}
