﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Character))]
[RequireComponent(typeof(Dialogue))]
[RequireComponent(typeof(Quest))]
public class NpcController : MonoBehaviour
{
    [SerializeField]
    public string npcName;
    Character character;
    public Transform dialogSign_Position;
    protected Collider2D _npcCollider;
    protected Collider2D _Collider;
    public DialogueSign dialogueSign;
    [HideInInspector]
    public bool isCurrentDialogue;
    //BookStatusCanvas bookStatusCanvas;
    private void Awake()
    {
        npcName = this.gameObject.name;
        //dialogSign_Position = transform.GetChild(0).GetComponentInChildren<Transform>();
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        _Collider = collider;
        Talk();
    }
    void OnTriggerExit2D(Collider2D collider)
    {
        UIGamePlay.Instance.SwicthControl(false);
    }
    void Talk()
    {
        if (CheckIfTalkable())
        {
            if (CheckIfHaveDialogue())
            {
                UIGamePlay.Instance.SwicthControl(true);
                UIGamePlay.Instance.interaction_BTN.onClick.RemoveAllListeners();
                UIGamePlay.Instance.interaction_BTN.onClick.AddListener(() => GameController.Instance.dialogueManager.StartDialogue());
            }
        }
    }
    bool CheckIfTalkable()
    {
        character = _Collider.GetComponent<Character>();
        if (character == null) { return false; }
        if (character.characterType == Character.CharacterTypes.Player)
        {
            return true;
        }
        return false;
    }
    public void ShowDialogSign()
    {
        DialogueSign m_dialogueSign = Resources.Load<DialogueSign>("Dialogue/Dialogue Sign");
        dialogueSign = Instantiate(m_dialogueSign, dialogSign_Position);
        dialogueSign.transform.SetParent(SceneBookStatus.Instance.dialogueCanvas.transform, false);
        dialogueSign.gameObject.transform.position = dialogSign_Position.position;
    }
    public void DestroyDialogSign()
    {
        isCurrentDialogue=false;
        if (dialogueSign != null)
        {
            Destroy(dialogueSign.gameObject);
        }
    }
    public bool CheckIfHaveDialogue()
    {
        if (isCurrentDialogue)
        {
            return true;
        }
        return false;
    }
}
