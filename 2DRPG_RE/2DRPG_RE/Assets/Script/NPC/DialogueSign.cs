﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueSign : MonoBehaviour
{
    public Image dialogSign;
    
    void Awake()
    {
        dialogSign = GetComponentInChildren<Image>();
        dialogSign.sprite = Resources.Load<Sprite>("Dialogue/Sign");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
