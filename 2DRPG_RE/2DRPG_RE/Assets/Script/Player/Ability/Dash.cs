﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dash : Ability
{
    [Header("Dash")]
    public float dashDistance = 3f;
    public float dashForce = 40f;
    protected float currentDashDistance;

    [Header("Cooldown")]
    public float dashCooldown = 1f;

    protected float _cooldownTimeStamp = 0;
    protected float _startTime;
    protected Vector3 _initialPosition;
    protected Vector2 lastmoveDir;
    protected Vector3 _dashDirection;
    public bool useSkill = false;
    // Start is called before the first frame update
    protected override void Initialization()
    {
        base.Initialization();
        Physics2D.queriesStartInColliders = false;
    }
    public override void ProcessAbility()
    {
        HandleInput();
        CheckLastDir();
    }
    public void SetDistance(float _canDashDistance)
    {
        currentDashDistance = _canDashDistance;
    }
    void CheckLastDir()
    {
        lastmoveDir = new Vector2(movement.m_movement.x, movement.m_movement.y).normalized;
        _dashDirection = lastmoveDir;
    }
    public void StartDash()
    {
        if (CanDash() && _cooldownTimeStamp <= Time.time)
        {
            InitiateDash();
        }
    }
    public virtual void InitiateDash()
    {
        useSkill = true;
        _startTime = Time.time;
        _initialPosition = this.transform.position;
        _cooldownTimeStamp = Time.time + dashCooldown;
        Vector3 dashPosition = transform.position + (_dashDirection * currentDashDistance);
        LeanTween.move(this.gameObject, dashPosition, 0.1f);
        StartCoroutine(Dashing(0.1f));
    }
    IEnumerator Dashing(float _protectiontime)
    {
        yield return new WaitForSeconds(_protectiontime);
        useSkill = false;
    }
    protected virtual void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            StartDash();
        }
    }
    private bool CanDash()
    {
        if (shield.useSkill)
        {
            return false;
        }
        if (PlayerController.Instance.player.GetComponent<Health>().RespawnAtInitialLocation)
        {
            return false;
        }
        if (PlayerController.Instance.player.GetComponent<Health>().CurrentHealth <= 0) { return false; }
        else { return true; }
    }
    public void CheckDashBTN()
    {
        Debug.Log("dash");
    }
}
