﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : Ability
{
    [Header("Protection Time")]
    public float protectiontime = 3f;
    [Header("Cooldown")]
    public float shieldCooldown = 1f;
    protected float _cooldownTimeStamp = 0;
    protected float _startTime;
    public bool useSkill = false;

    protected override void Initialization()
    {
        base.Initialization();
    }
    public override void ProcessAbility()
    {
        HandleInput();

    }
    public override void EarlyProcessAbility()
    {
        ShieldAnimation();
    }
    public void ShieldAnimation()
    {
        if (useSkill)
        {
            animator.SetBool("Shield", true);
        }
        else
        {
            animator.SetBool("Shield", false);            
        }
    }
    public void UseShield()
    {
        if (_cooldownTimeStamp <= Time.time)
        {
            ShieldUp();
        }
        else { useSkill = false; }
    }
    public void ShieldUp()
    {
        _startTime = Time.time;
        _cooldownTimeStamp = Time.time + shieldCooldown;
        useSkill = true;
        StartCoroutine(Shielding(protectiontime));
    }
    IEnumerator Shielding(float _protectiontime)
    {
        yield return new WaitForSeconds(_protectiontime);
        useSkill = false;
    }
    protected virtual void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            UseShield();
        }
    }
}
