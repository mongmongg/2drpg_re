﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Character))]
//[RequireComponent(typeof(CharacterDash))]
//[RequireComponent(typeof(CharacterShield))]
public class Ability : MonoBehaviour
{
    Character character;
    protected Rigidbody2D rb;
    [HideInInspector]
    public Vector2 m_movement;

    protected Animator animator;
    protected Movement movement;
    protected Shield shield;
    protected Dash characterDash;
    protected Health health;
    protected bool wontHurt;
    protected virtual void Start()
    {
        Initialization();
    }
    protected virtual void Initialization()
    {
        character = GetComponent<Character>();
        rb = GetComponent<Rigidbody2D>();
        movement = GetComponent<Movement>();
        shield = GetComponent<Shield>();
        characterDash = GetComponent<Dash>();
        health = GetComponent<Health>();
        animator = GetComponent<Animator>();
    }
    void FixedUpdate()
    {
        EarlyProcessAbility();
        
    }
    private void Update()
    {
        ProcessAbility();
    }
    //Update
    public virtual void ProcessAbility()
    {

    }

    //FixedUpdate
    public virtual void EarlyProcessAbility()
    {

    }
   
}
