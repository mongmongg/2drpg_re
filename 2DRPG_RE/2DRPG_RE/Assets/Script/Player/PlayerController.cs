﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : SingletonBehaviour<PlayerController>
{
    Character character;
    Dash dash;
    Shield shield;
    public GameObject player;
    void Awake()
    {
        character = Instantiate(Resources.Load<Character>("Prefabs/Character/Player"), transform);
        player = character.gameObject;
        GameController.Instance.playerController = this;
        dash = character.GetComponent<Dash>();
        shield = character.GetComponent<Shield>();
        SetActionButton();
    }
    void SetActionButton()
    {
        UIGamePlay.Instance.dash_BTN.onClick.AddListener(() => dash.StartDash());
        UIGamePlay.Instance.shield_BTN.onClick.AddListener(() => shield.UseShield());
        UIGamePlay.Instance.character_Icon.onClick.AddListener(() => UIGamePlay.Instance.InterfaceStatusPanel(Node.StatusInfo));
        UIGamePlay.Instance.bag_Icon.onClick.AddListener(() => UIGamePlay.Instance.InterfaceStatusPanel(Node.Inventory));
        UIGamePlay.Instance.SwicthControl(false);
    }
   
}
