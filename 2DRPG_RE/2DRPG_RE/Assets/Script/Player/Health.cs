﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Health : Ability
{
    public CharacterStatusPanel healthBar;

    [Header("Health")]
    public int CurrentHealth;
    public int InitialHealth;
    public int MaximumHealth;
    [Header("Death")]
    public bool DestroyOnDeath = true;
    public bool RespawnAtInitialLocation = false;

    private Material matDefault;
    private Material matHurt;
    SpriteRenderer spriteRenderer;

    protected override void Initialization()
    {
        base.Initialization();
        healthBar = UIGamePlay.Instance.character_Icon.gameObject.GetComponent<CharacterStatusPanel>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        matHurt = Resources.Load("Prefabs/Character/Hurt", typeof(Material)) as Material;
        matDefault = spriteRenderer.material;
        UpdateHealthBar(true);
        if (CurrentHealth == 0)
        {
            SetHealth(InitialHealth);
        }
    }
    public virtual void SetHealth(int _health)
    {
        CurrentHealth = _health;
        UpdateHealthBar(true);
    }
    public virtual void TakeDamage(int _damage)
    {
        if (CheckCanTakeDamage())
        {
            spriteRenderer.material = matHurt;
            CurrentHealth -= _damage;
            UpdateHealthBar(true);
            if (CurrentHealth <= 0)
            {
                Debug.Log("Dead!!!");
                ResetMaterial();
                animator.SetBool("Death", true);
                RespawnAtInitialLocation = true;
                StartCoroutine(WaitForReSpawn(3f));
            }
            else
            {
                Invoke("ResetMaterial", .3f);
            }
        }
    }
    protected virtual void UpdateHealthBar(bool show)
    {
        if (healthBar != null)
        {
            healthBar.UpdateBar(CurrentHealth, 0f, MaximumHealth, show);
        }
    }
    void ResetMaterial()
    {
        spriteRenderer.material = matDefault;
    }
    IEnumerator WaitForReSpawn(float _time)
    {
        PlayerPrefs.SetString("SceneNumber", SceneManager.GetActiveScene().name);
        yield return new WaitForSeconds(_time);
        UIGamePlay.Instance.fadeImage.FadeOut();
        yield return new WaitForSeconds(0.5f);
        Respawn();
    }
    void Respawn()
    {
        GameController.Instance.saveManager.LoadData();
        animator.SetBool("Death", false);
        RespawnAtInitialLocation = false;
    }
    public void PlayerDead()
    {
        CurrentHealth = 0;        
        animator.SetBool("Death", true);
        UpdateHealthBar(true);
    }
    bool IsDeath()
    {
        if (CurrentHealth <= 0)
        {
            return true;
        }
        else { return false; }
    }
    bool CheckCanTakeDamage()
    {
        if (shield.useSkill)
        {
            return false;
        }
        if (characterDash.useSkill)
        {
            return false;
        }
        if (IsDeath())
        {
            return false;
        }
        else { return true; }
    }
}
